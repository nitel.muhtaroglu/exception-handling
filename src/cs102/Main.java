package cs102;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    private static int[] array = {10, 45, 67, 3, 98};

    public static void main(String[] args) {
        Main.first();
    }

    public static void first() {
        try {
            Main.second();
        } catch (ArrayIndexOutOfBoundsException exception) {
            System.out.println("Out of bounds. Try again!");
            Main.first();
        }
    }

    public static void second() {
        try {
            Main.third();
        } catch (InputMismatchException exception) {
            System.out.println("Improper input. Try again!");
            Main.second();
        } catch (ArithmeticException exception) {
            System.out.println("Division by zero. Try again!");
            Main.second();
        }
    }

    public static void third() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a: ");
        int a = scanner.nextInt();
        System.out.print("Enter b: ");
        int b = scanner.nextInt();
        System.out.println(Main.array[a / b]);
    }
}
